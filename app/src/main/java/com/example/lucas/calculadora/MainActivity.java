package com.example.lucas.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etOperando1;
    private EditText etOperando2;
    private RadioGroup rgOperacoes;
    private TextView tvResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etOperando1 = (EditText) findViewById(R.id.etOperando1);
        etOperando2 = (EditText) findViewById(R.id.etOperando2);
        rgOperacoes = (RadioGroup) findViewById(R.id.rgOperacoes);
        tvResultado = (TextView) findViewById(R.id.tvResultado);
    }

    public void calcular(View quemDisparou) {
        double operando1, operando2, resultado;
        String operacao;

        String strOperando1 = etOperando1.getText().toString();
        String strOperando2 = etOperando2.getText().toString();
        etOperando1.setError(null);
        etOperando2.setError(null);

        try{
            operando1 = Double.parseDouble(strOperando1);
        } catch (Exception e) {
            etOperando1.setError("Falha na conversão");
            return;
        }

        try{
            operando2 = Double.parseDouble(strOperando2);
        }catch (Exception e) {
            etOperando2.setError("Falha na conversão");
            return;
        }

        operando1 = Double.parseDouble(strOperando1);
        operando2 = Double.parseDouble(strOperando2);

        operacao = ((RadioButton) findViewById(rgOperacoes.getCheckedRadioButtonId())).getText().toString();

        if(operacao.equals("Soma")) {
            resultado = operando1 + operando2;
            String resultadoLegivel = getResources().getString(R.string.resultado, resultado);
            tvResultado.setText(resultadoLegivel);
        } else if(operacao.equals("Subtração")) {
            resultado = operando1 - operando2;
            String resultadoLegivel = getResources().getString(R.string.resultado, resultado);
            tvResultado.setText(resultadoLegivel);
        } else if (operacao.equals("Multiplicação")) {
            resultado = operando1 * operando2;
            String resultadoLegivel = getResources().getString(R.string.resultado, resultado);
            tvResultado.setText(resultadoLegivel);
        } else if (operacao.equals("Divisão")) {
            if (strOperando2.equals("0")) {
                etOperando2.setError("Impossível dividir por zero");
                tvResultado.setText("");
            } else {
                resultado = operando1 / operando2;
                String resultadoLegivel = getResources().getString(R.string.resultado, resultado);
                tvResultado.setText(resultadoLegivel);
            }
        }
    }
}
